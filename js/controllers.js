app.controller("AlarmCtrl", function ($scope, SharedData, alarmRequest, $filter, $rootScope, $window, $document, $timeout) {

    //reference parent scope
    var vm = this;

    //boolean to show modal on load (show if user has not selected any products)
    this.userPrefShow = SharedData.isLocalStorageEmpty();

    //function to reset amount of alarms being showed in the list
    var listCount = 50;
    this.resetShowAmount = function () {
        this.showAmountObj = {
            master: listCount,
            shown: listCount
        };
    };

    //instantiate show amount
    this.resetShowAmount();

    //button to show more
    this.showMore = function () {
        this.showAmountObj.shown += this.showAmountObj.master;
    };

    //collapsed mobile view on load
    this.collapse = false;

    //list of alarms - master (left side)
    this.masterAlarmList = null;

    //filtered list
    this.filteredAlarmList = null;

    //list for strings being requested
    this.alarmStrings = null;

    //array to store additional data (troubleshooting/occurs/info)
    var additionalData = [];

    //isString
    this.isString = angular.isString;

    //url to send out
    this.currentUrl = null;

    //function to filter data (text input or click to show search filters (prompts, safetys))
    this.filterData = function (text) {
        var filtered = $filter('filter')(this.masterAlarmList, text);
        if (this.searchFilters[this.selectedDevices[this.selectedIndex].product.shortName]) {
            filtered = $filter('alarmNameFilter')(filtered, this.searchFilters[this.selectedDevices[this.selectedIndex].product.shortName]);
        }
        this.filteredAlarmList = filtered;
    };

    //filter request for alarm list
    this.searchFilters = {
        optia: {
            lookupField: "enumerator",
            alarms: [{
                name: "prompt",
                value: "Prompt",
                checked: false,

            }, {
                name: "safety",
                value: "_",
                checked: false
            }]
        },
        trima: {
            lookupField: "dlog",
            alarms: [{
                name: "safety",
                value: "<Safety>",
                checked: false
            }]
        }
    };


    //function to reset alarm
    this.resetAlarm = function () {
        this.alarmObj = {
            alarm: null,
            search: ""
        };
    };

    //load alarmObj on page load
    this.resetAlarm();

    //request JSON data
    this.request = function (s, i) {
        additionalData = [];
        alarmRequest.request(s, i).then(function (resp) {
            angular.forEach(resp, function (v, i) {
                switch (i) {
                case 0:
                    var filterField = "name";
                    if ("number" in v.data.alarms[0]) {
                        angular.forEach(v.data.alarms, function (v) {
                            v.number = Number(v.number);
                        });
                        filterField = "number";
                    }
                    vm.masterAlarmList = vm.filteredAlarmList = $filter('orderBy')(v.data.alarms, filterField);
                    angular.forEach(vm.masterAlarmList, function (v, i) {
                        v.alarmIndex = i;
                    });
                    break;
                default:
                    additionalData.push(v.data);
                    break;
                }
            });
        }).then(function () {
            vm.filterData();
            if (vm.currentAlarmIndex) {

                angular.forEach(vm.masterAlarmList, function (v, i) {
                    if (v.enumerator === vm.currentAlarmIndex) {
                        vm.currentAlarmIndex = i;
                    }
                });

                if (vm.masterAlarmList[vm.currentAlarmIndex]) {
                    vm.setAlarm(vm.masterAlarmList[vm.currentAlarmIndex]);
                    if (vm.currentAlarmIndex > vm.showAmountObj.shown) {
                        vm.showAmountObj.shown = vm.currentAlarmIndex + 1;
                    }
                }
            }
        });
    };

    //set selected devices index
    this.setIndex = function (i) {
        this.selectedIndex = i;
        SharedData.setSelectedIndex(i);
    };

    //function to get alarm strings
    this.getAlarmStrings = function () {
        SharedData.getAlarmStrings(this.selectedDevices[this.selectedIndex].language.langCode).then(function (resp) {
            vm.alarmStrings = resp.data.strings;
        });
    };

    //set new device
    this.setNewDevice = function (s, i) {
        if (angular.isArray(s)) {
            s = s[i];
        }

        this.resetAlarm();
        this.setIndex(i);
        this.request(s);
        this.getAlarmStrings();
        this.resetShowAmount();
        this.currentAlarmIndex = null;
        this.currentUrl = null;
    };

    //set alarm on right side (view alarm details)
    this.setAlarm = function (alarm) {
        if (angular.equals(this.alarmObj.alarm, alarm)) {
            return;
        }
        var key = alarm.enumerator ? "enumerator" : "alarmId";
        angular.forEach(additionalData, function (v) {
            angular.forEach(v, function (v2, i2) {
                alarm[i2] = SharedData.filterAddtionalData(key, alarm[key], v2, vm.selectedDevices[vm.selectedIndex].product.shortName);
            });
        });

        this.alarmObj.alarm = alarm;
        this.currentAlarmIndex = alarm.alarmIndex;
        this.currentUrl = null;

        if ($scope.isMobile) {
            this.collapse = true;
        }
    };

    //function to update users selcted product/version/languages
    this.currentAlarmIndex = null;
    this.updateDeviceList = function (p) {
        this.selectedDevices = p.devices;
        this.setNewDevice(this.selectedDevices, p.index);
        if (p.alarmIndex) {
            this.currentAlarmIndex = p.alarmIndex;
        }
    };

    this.getCustomUrl = function () {
        var current = this.selectedDevices[this.selectedIndex];
        this.currentUrl = $window.location.protocol + '//' + $window.location.host + $window.location.pathname + "?p=" + current.product.masterIndex + "&v=" + current.version.masterIndex + "&l=" + current.language.masterIndex + "&idx=" + this.alarmObj.alarm.enumerator;
        var t = $timeout(function () {
            $document[0].getElementById("current-url").select();
            $timeout.cancel(t);
        });

    };
});