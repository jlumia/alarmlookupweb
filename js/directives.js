app.directive("modal", function (SharedData, $timeout) {
    return {
        restrict: "E",
        scope: {
            show: "=",
            updateDeviceList: "&updateFn"
        },
        controllerAs: "vm",
        controller: function ($scope) {

            //set this
            var vm = this;

            this.alarmIndex = false;

            //function to get product list
            SharedData.getProductList().then(function (resp) {
                vm.productList = resp;
            }).then(function () {
                if (SharedData.getUrlVars()["p"] && SharedData.getUrlVars()["v"] && SharedData.getUrlVars()["l"]) {
                    if (vm.productList[+SharedData.getUrlVars()["p"]]) {
                        if (vm.productList[+SharedData.getUrlVars()["p"]].versions[+SharedData.getUrlVars()["v"]]) {
                            if (vm.productList[+SharedData.getUrlVars()["p"]].versions[+SharedData.getUrlVars()["v"]].languages[+SharedData.getUrlVars()["l"]]) {
                                vm.productList[+SharedData.getUrlVars()["p"]].versions[+SharedData.getUrlVars()["v"]].languages[+SharedData.getUrlVars()["l"]].checked = true;
                                vm.alarmIndex = true;
                            }
                        }
                    }
                }
            }).then(function () {
                var t = $timeout(function () {
                    vm.update();
                    $timeout.cancel(t);
                }, 0);
            });

            //function to clear product list (delete array while modal is hidden - reduces $$watchersCount tremedously, increasing performance)
            this.setLengthLimit = function (n) {
                this.lengthLimit = n;
            };

            //if user has changed device selection
            this.selectedCopy = null;
            this.devicesHaveChanged = function () {
                return !angular.equals(this.deviceArray, this.selectedCopy);
            };

            //function to update the device array
            var load = false;
            this.update = function () {
                this.deviceArray = [];
                for (var i = 0; i < this.productList.length; i++) {
                    for (var j = 0; j < this.productList[i].versions.length; j++) {
                        for (var k = 0; k < this.productList[i].versions[j].languages.length; k++) {
                            if (this.productList[i].versions[j].languages[k].checked) {
                                var obj = {
                                    product: {
                                        fullName: this.productList[i].fullName,
                                        shortName: this.productList[i].shortName,
                                        masterIndex: i
                                    },
                                    version: {
                                        fullName: this.productList[i].versions[j].fullName,
                                        shortName: this.productList[i].versions[j].shortName,
                                        masterIndex: j
                                    },
                                    language: {
                                        langCode: this.productList[i].versions[j].languages[k].langCode,
                                        langName: this.productList[i].versions[j].languages[k].langName,
                                        masterIndex: k
                                    },
                                };
                                this.deviceArray.push(obj);
                            }
                        }
                    }
                }

                //set local storage
                SharedData.setDeviceList(this.productList);
                if (vm.alarmIndex && !load) {
                    $scope.show = false;
                }
                load = true;
            };

        },
        link: function ($scope, $element, $attrs, $ctrl) {

            $element.ready(function () {

                //click off modal to close
                var modalClick = function (e) {
                    if (e.target.className.indexOf("modal-show") > -1) {
                        if ($ctrl.deviceArray.length) {
                            $scope.show = false;
                            $scope.$apply();
                        } else {
                            alert("You must select a device");
                        }
                    }
                };

                //boolean to check if modal has previously been show
                var isLoaded = false;

                //watch show for open/close
                $scope.$watch("show", function (n) {

                    //on modal show
                    if (n) {

                        //bind click off
                        $element.bind("click", modalClick);

                        //create a copy to check if deviceList changed
                        $ctrl.selectedCopy = angular.copy($ctrl.deviceArray);

                        //modal has opened
                        isLoaded = true;

                        //request list
                        if ($ctrl.productList) {
                            $ctrl.setLengthLimit($ctrl.productList.length);
                        }

                    } else {

                        //unbind click off event
                        $element.unbind("click", modalClick);

                        //send data to controller with device list (populates dropdown)
                        var indexToSend = 0;
                        if ($ctrl.alarmIndex) {
                            var psn = $ctrl.productList[+SharedData.getUrlVars()["p"]].shortName;
                            var vsn = $ctrl.productList[+SharedData.getUrlVars()["p"]].versions[+SharedData.getUrlVars()["v"]].shortName;
                            var llc = $ctrl.productList[+SharedData.getUrlVars()["p"]].versions[+SharedData.getUrlVars()["v"]].languages[+SharedData.getUrlVars()["l"]].langCode;
                            angular.forEach($ctrl.deviceArray, function (v, i) {
                                if (v.product.shortName === psn && v.version.shortName === vsn && v.language.langCode === llc) {
                                    indexToSend = i;
                                }
                            });
                        } else {
                            indexToSend = $ctrl.devicesHaveChanged() && isLoaded ? 0 : SharedData.getSelectedIndex();
                        }

                        $scope.updateDeviceList({
                            device: {
                                devices: $ctrl.deviceArray,
                                index: indexToSend,
                                alarmIndex: $ctrl.alarmIndex ? SharedData.getUrlVars()["idx"] : null
                            }
                        });

                        //clear list
                        $ctrl.setLengthLimit(0);
                    }
                });
            });
        },
        templateUrl: "lib/templates/modal.html"
    };
});

app.directive("content", function ($document, $window) {
    return {
        restrict: "A",
        link: function ($scope, $element) {

            //function to calculate height of content (everything under header) to keep page from scrolling
            var getHeight = function () {
                var alarmListCalculated = "calc(100% - " + ($document[0].getElementsByClassName("heading")[0].clientHeight + 2) + "px)";
                $element.css("height", alarmListCalculated);
                $scope.isMobile = $window.innerWidth <= 768 ? true : false;
                if (!$scope.isMobile) {
                    alarmListCalculated = "100%";
                }
                angular.element($element[0].getElementsByClassName("alarm-list-side")).css("height", alarmListCalculated);

            };
            getHeight();
            angular.element($window).bind("resize", function () {
                getHeight();
                $scope.$digest();
            });
        }
    };
});

app.directive("deviceDropdown", function ($window, $document, $timeout) {
    return {
        restrict: "A",
        scope: true,
        link: function ($scope, $element) {

            //dropdown is open boolean
            $scope.ddIsOpen = false;

            //funcion to calculate height of dropdown for sliding
            var getHeight = function () {
                $element.find("dropdown").css("maxHeight", $element.find("dropdown").children()[0].clientHeight + "px");
            };

            //minimize dropdown on off click
            var offClick = function (e) {
                if (e.target.className !== "each-dd") {
                    $scope.openDropdown();
                    $scope.$digest();
                }
            };

            //function to open/close dropdown
            $scope.openDropdown = function () {
                $scope.ddIsOpen = !$scope.ddIsOpen;
                if ($scope.ddIsOpen) {
                    getHeight();
                    var t = $timeout(function () {
                        $document.bind("click", offClick);
                        $timeout.cancel(t);
                    }, 100);
                } else {
                    $document.unbind("click", offClick);
                }
            };

            angular.element($window).bind("resize", function () {
                if ($scope.ddIsOpen) {
                    getHeight();
                }
            });

        }
    };
});

app.directive("alarmList", function ($window, $timeout) {
    return {
        restrict: "E",
        link: function ($scope, $element) {

            //calculate alarm list height to have only that section scrollable
            var getHeight = function () {
                $element.css("height", "calc(100% - " + $element.parent().find("dd-menu")[0].clientHeight + "px)");
            };

            //resize everytime the alarm master list repopulates
            $scope.$watch("alarm.masterAlarmList", function (n) {
                if (n) {
                    var t = $timeout(function () {
                        getHeight();
                        $timeout.cancel(t);
                    }, 0);
                }
            });

            angular.element($window).bind("resize", function () {
                getHeight();
            });
        }
    };
});