//capitalize first letter
app.filter("capitalize", function () {
    return function (input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : "";
    };
});

// filter by search strings
app.filter("alarmNameFilter", function () {
    return function (alarms, searchFilters) {
        var filtered = null;
        if (alarms && searchFilters) {
            var f = [];
            angular.forEach(searchFilters.alarms, function (v2) {
                if (!v2.checked) {
                    f.push(v2.value);
                }
            });
            filtered = alarms.filter(function (alarm) {
                for (var i in f) {
                    if (alarm[searchFilters.lookupField]) {
                        if (alarm[searchFilters.lookupField].indexOf(f[i]) !== -1) {
                            return false;
                        }
                    }
                }
                return true;
            });
        }

        if (filtered) {
            return filtered;
        }

        return alarms;
    };
});

// convert /n to <br>
// copied from alarmlookuponsen; probably needs modification to work here.
app.filter("nl2br", function ($sce) {
    return function (data) {
        if (!data) {
            return data;
        }
        return $sce.trustAsHtml(data.replace(/\n\r?/g, '<br>'));
    };
});