app.service("SharedData", function ($window, $http, $q, productRequest) {

    //selected device index
    var selectedIndex = Number($window.localStorage.getItem("ALT_Selected_Index")) || 0;

    //check for localStorage
    var isLocalStorageEmpty = function () {
        return $window.localStorage.getItem("ALT_Device_List") === null;
    };

    //either request the data or get it from localStorage
    var getProductList = function () {
        if (this.getDeviceList()) {
            var defer = $q.defer();
            defer.resolve(this.getDeviceList());
            return defer.promise;
        }

        return productRequest.request();
    };

    //get localStorage if it exists (for device select dropdown)
    var getDeviceList = function () {
        return JSON.parse($window.localStorage.getItem("ALT_Device_List"));
    };

    //set the product list array
    var setDeviceList = function (arr) {
        $window.localStorage.setItem("ALT_Device_List", JSON.stringify(arr));
    };

    //set selected index
    var setSelectedIndex = function (i) {
        $window.localStorage.setItem("ALT_Selected_Index", i);
        selectedIndex = i;
    };

    //get selected index
    var getSelectedIndex = function () {
        return selectedIndex;
    };

    //filter additional data requested when user picks an alarm
    var filterAddtionalData = function (filterValue, filterField, data, prod) {
        if (prod === "mirasol") {
            var n = filterField.match(/\d/g);
            filterField = filterField.substring(0, filterField.lastIndexOf(n[n.length - 1]) + 1);
        }
        var a = data.filter(function (x) {
            return x[filterValue] == filterField;
        });
        return a[0];
    };

    //get the alarm strings
    var getAlarmStrings = function (lang) {
        return $http.get("data/strings_" + lang + ".json");
    };

    //get variables from the URL
    var getUrlVars = function () {
        var vars = {};
        $window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            vars[key] = value;
        });
        return vars;
    };

    return {
        isLocalStorageEmpty: isLocalStorageEmpty,
        getProductList: getProductList,
        getDeviceList: getDeviceList,
        setDeviceList: setDeviceList,
        setSelectedIndex: setSelectedIndex,
        getSelectedIndex: getSelectedIndex,
        filterAddtionalData: filterAddtionalData,
        getAlarmStrings: getAlarmStrings,
        getUrlVars: getUrlVars
    };
});