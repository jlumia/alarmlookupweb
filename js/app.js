var app = angular.module("AlarmLookupTool", ['ngSanitize']).config(function ($compileProvider) {
    
    //removes debug/class tags
    $compileProvider.debugInfoEnabled(false);
});