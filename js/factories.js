//request products for users to select devices (product, version, language modal)
app.factory("productRequest", function ($http, $q) {
    return {
        request: function () {
            var mainObj = [];
            return $http.get('data/products.json').then(function (resp) {
                var versionArray = [];
                angular.forEach(resp.data.prodList, function (v) {
                    mainObj.push(v);
                    versionArray.push($http.get("data/" + v.shortName + "/" + v.shortName + "_versions.json"));
                });
                return $q.all(versionArray);
            }).then(function (resp) {
                var languageArray = [];
                angular.forEach(resp, function (v, i) {
                    angular.extend(mainObj[i], {
                        versions: v.data.verList
                    });
                    angular.forEach(v.data.verList, function (v2) {
                        languageArray.push($http.get("data/" + mainObj[i].shortName + "/" + v2.shortName + "/" + v2.shortName + "_languages.json"));
                    });
                });
                return $q.all(languageArray);
            }).then(function (resp) {
                var count = 0;
                angular.forEach(mainObj, function (v) {
                    angular.forEach(v.versions, function (v2) {
                        angular.extend(v2, {
                            languages: resp[count].data.langList
                        });
                        count++;
                    });
                });
                return mainObj;
            });
        }
    };
});

//get alarm
app.factory("alarmRequest", function ($http, $q) {
    return {
        request: function (s, i) {
            var ajaxArray = [];
            var selectedDevice = i >= 0 ? s[i] : s;
            ajaxArray.push($http.get("data/" + selectedDevice.product.shortName + "/" + selectedDevice.version.shortName + "/" + selectedDevice.version.shortName + "_alarms_" + selectedDevice.language.langCode + ".json"));
            switch (selectedDevice.product.shortName) {
            case "trima":
                var search = selectedDevice.version.shortName.search(/\d/);
                ajaxArray.push($http.get("data/" + selectedDevice.product.shortName + "/" + selectedDevice.product.shortName + "_troubleshooting_" + selectedDevice.version.shortName[search] + ".json"));
                break;
            case "optia":
                ajaxArray.push($http.get("data/" + selectedDevice.product.shortName + "/" + selectedDevice.product.shortName + "_troubleshooting.json"));
                ajaxArray.push($http.get("data/" + selectedDevice.product.shortName + "/" + selectedDevice.product.shortName + "_occurs.json"));
                break;
            case "quantum":
                ajaxArray.push($http.get("data/" + selectedDevice.product.shortName + "/" + selectedDevice.product.shortName + "_info.json"));
                break;
            default:
                ajaxArray.push($http.get("data/" + selectedDevice.product.shortName + "/" + selectedDevice.product.shortName + "_troubleshooting.json"));
            }
            return $q.all(ajaxArray);
        }
    };
});